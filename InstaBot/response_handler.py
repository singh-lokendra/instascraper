import json

import playwright
from playwright.async_api import Response


class ResponseHandler:
    def __init__(self, scroll_count=5):
        self._json_response: list[Response] = []
        self.scroll_count = scroll_count
        self._response_body = None

    def capture_response(self, response: Response):
        query_hash = 'https://www.instagram.com/graphql/query/?query_hash'
        if response.url.startswith(query_hash):
            self._json_response.append(response)

    @property
    def response_body(self) -> dict[str, str]:
        """
        list of captured responses

        :return: dict[str]
        """
        if self._response_body is None:
            self._response_body = {}
            for i, response in enumerate(self._json_response):
                try:
                    clean_response = response.body().decode()

                    deserialize_response = json.loads(clean_response)
                    self._response_body[str(i)] = deserialize_response
                except playwright._impl._api_types.Error:
                    pass
        return self._response_body
