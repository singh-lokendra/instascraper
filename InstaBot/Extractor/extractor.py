import json
import os
from datetime import datetime
from types import SimpleNamespace
from typing import Iterator, List

from InstaBot.Extractor.reader import read_html, extract_response_data
from InstaBot.Extractor.schema import (
    RelatedProfiles,
    UserDetails,
    PostDetails,
    Owner,
    Video,
    IGTV,
)


class ExtractInsta:
    def __init__(self, page_source: str):
        self.page_source = page_source
        self._json_post = None
        self._post = None
        self._post_edges = None
        self._igtv_edges = None
        self._user = None
        self._html_content = None

    @property
    def json_post(self):
        if not self._json_post:
            json_post = extract_response_data(self.page_source)
            self._json_post = json.dumps(json_post, indent=4)

        return self._json_post

    @property
    def post(self) -> SimpleNamespace:
        if not self._post:
            self._post = json.loads(self.json_post, object_hook=lambda d: SimpleNamespace(**d))
        return self._post

    @property
    def post_edges(self) -> List[SimpleNamespace]:
        if not self._post_edges:
            self._post_edges = self.post[0].graphql.user.edge_owner_to_timeline_media.edges
        return self._post_edges

    @property
    def igtv_edges(self) -> List[SimpleNamespace]:
        if not self._igtv_edges:
            self._igtv_edges = self.post[0].graphql.user.edge_felix_video_timeline.edges
        return self._igtv_edges

    @property
    def user(self) -> SimpleNamespace:
        if not self._user:
            self._user = self.post[0].graphql.user
        return self._user

    def extract_related_users(self):
        edge_related_profiles = self.user.edge_related_profiles.edges
        yield from related_users(edge_related_profiles)

    def extract_user_details(self) -> UserDetails:
        user = self.user
        post = self.post

        serialize_user = UserDetails(
            insta_id=user.id,
            username=user.username,
            full_name=user.full_name,
            profile_pic_url=user.profile_pic_url,
            is_private=user.is_private,
            is_verified=user.is_verified,
            fbid=user.fbid,
            biography=user.biography,
            external_url=user.external_url,
            followers=user.edge_followed_by.count,
            business_account=user.is_business_account,
            professional_account=user.is_professional_account,
            business_email=user.business_email,
            business_phone_no=user.business_phone_number,
            profile_pic_hd=user.profile_pic_url_hd,
            business_category_name=user.business_category_name,
            category_name=user.category_name,
            seo_category_infos=post[0].seo_category_infos,
            # related_users=list(self.extract_related_users()),
        )
        return serialize_user

    def extract_post_details(self) -> Iterator[PostDetails]:
        yield from extract_posts(self.post_edges)

    def extract_igtv_details(self) -> Iterator[PostDetails]:
        yield from extract_igtv(self.igtv_edges)


def main():
    script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
    rel_path = "user-profiles/"
    abs_file_path = os.path.join(script_dir, rel_path)

    html_content = read_html(abs_file_path)
    e = ExtractInsta(html_content)

    user_details = e.extract_user_details()
    posts = list(e.extract_post_details())
    igtv = list(e.extract_igtv_details())
    print(user_details)
    print(posts)
    print(igtv)


def extract_posts(post_edges: List[SimpleNamespace]) -> Iterator[PostDetails]:
    for post in post_edges:
        post = post.node
        owner = Owner(
            insta_id=post.owner.id,
            username=post.owner.username,
        )

        video_view_count = None
        if post.is_video:
            video_view_count = post.video_view_count

        video = Video(
            is_video=post.is_video,
            video_view_count=video_view_count,
        )
        location = None
        if post.location:
            location = post.location.name

        timestamp = datetime.utcfromtimestamp(post.taken_at_timestamp)
        post_details = PostDetails(
            insta_id=post.id,
            display_url=post.display_url,
            owner=owner,
            video=video,
            caption=post.edge_media_to_caption.edges[0].node.text,
            comment=post.edge_media_to_comment.count,
            likes=post.edge_media_preview_like.count,
            timestamp=timestamp,
            location=location,
            accessibility_caption=post.accessibility_caption,
        )
        yield post_details


def extract_igtv(igtv_edges: List[SimpleNamespace]) -> Iterator[PostDetails]:
    for post in igtv_edges:
        post = post.node
        owner = Owner(
            insta_id=post.owner.id,
            username=post.owner.username,
        )

        video = Video(
            is_video=post.is_video,
            video_view_count=post.video_view_count,
            duration=post.video_duration,
        )

        location = None
        if post.location:
            location = post.location.name

        timestamp = datetime.utcfromtimestamp(post.taken_at_timestamp)

        igtv_details = IGTV(
            insta_id=post.id,
            display_url=post.display_url,
            owner=owner,
            video=video,
            caption=post.edge_media_to_caption.edges[0].node.text,
            comment=post.edge_media_to_comment.count,
            likes=post.edge_media_preview_like.count,
            timestamp=timestamp,
            location=location,
            accessibility_caption=post.accessibility_caption,
            title=post.title,
        )
        yield igtv_details


def related_users(edge_related_profiles):
    for related_profile in edge_related_profiles:
        user = related_profile.node
        yield RelatedProfiles(
            insta_id=user.id,
            username=user.username,
            full_name=user.full_name,
            profile_pic_url=user.profile_pic_url,
            is_private=user.is_private,
            is_verified=user.is_verified,
        )


if __name__ == '__main__':
    main()
