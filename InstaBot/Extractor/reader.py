import json
import re
from typing import Dict, List


def read_html(file=None) -> str:
    if not file:
        file = 'user-profiles/shachi.html'

    return open(file).read()


def extract_response_data(html) -> List[Dict]:
    """
    Copy logic from instaloader package

    https://github.com/instaloader/instaloader/blob/8a2694321539d7f0d6c95f40896a1e70adc08246/instaloader/instaloadercontext.py#L353-L367
    if things break in future, refer the changes in above segment

    :return:
    """

    match = re.search(r'window\._sharedData = (.*);</script>', html)
    if match is None:
        raise ValueError('Could not find "window._sharedData" in html response.')
    resp_json = json.loads(match.group(1))
    entry_data = resp_json.get('entry_data')
    post_or_profile_page = list(entry_data.values())[0] if entry_data is not None else None
    if post_or_profile_page is None:
        raise ValueError('"window._sharedData" does not contain required keys.')

    # If GraphQL data is missing in `window._sharedData`, search for it in `__additionalDataLoaded`.
    if 'graphql' not in post_or_profile_page[0]:
        match = re.search(r'window\.__additionalDataLoaded\(.*?({.*"graphql":.*})\);</script>',
                          html)
        if match is not None:
            post_or_profile_page[0]['graphql'] = json.loads(match.group(1))['graphql']
    return post_or_profile_page


if __name__ == '__main__':
    html_content = read_html()
    data = extract_response_data(html_content)
    print(data)
