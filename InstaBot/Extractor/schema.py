from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional, List, Any


@dataclass
class Owner:
    insta_id: int
    username: str


@dataclass
class Video:
    is_video: bool = False
    video_view_count: int = 0
    duration: float = 0.0


@dataclass
class Post:
    insta_id: int = field(repr=False)
    likes: str
    comment: int
    caption: str = field(repr=False)
    owner: Owner = field(repr=False)
    timestamp: datetime = field(repr=False)
    video: Video = field(repr=False)
    accessibility_caption: Optional[str] = field(repr=False)
    location: Optional[str] = field(repr=False)
    display_url: str = field(repr=False)


@dataclass
class PostDetails(Post):
    pass


@dataclass
class IGTV(Post):
    title: str = ''


@dataclass
class User:
    insta_id: str
    username: str
    full_name: str = field(repr=False)
    profile_pic_url: str = field(repr=False)
    is_private: bool = field(repr=False)
    is_verified: bool = field(repr=False)


@dataclass
class RelatedProfiles(User):
    pass


@dataclass
class UserDetails(User):
    """
    Response class
    """
    fbid: str = field(repr=False)

    seo_category_infos: List[Any] = field(repr=False, default=None)
    biography: Optional[str] = field(repr=False, default=None)
    external_url: Optional[str] = field(repr=False, default=None)
    followers: Optional[int] = field(repr=False, default=None)
    business_account: Optional[bool] = field(repr=False, default=None)
    professional_account: Optional[bool] = field(repr=False, default=None)

    business_email: Optional[str] = None
    business_phone_no: Optional[str] = None

    profile_pic_hd: Optional[str] = field(repr=False, default=None)

    business_category_name: Optional[str] = field(repr=False, default=None)
    category_name: Optional[str] = field(repr=False, default=None)
    related_users: Optional[List[RelatedProfiles]] = field(repr=False, default=None)
